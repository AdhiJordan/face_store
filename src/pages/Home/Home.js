import React, {Component} from 'react';
import styles from './home.scss';
import axios from 'axios';
import Header from './../../components/Header';
import Filters from './../../components/Filters';
import Products from './../../components/Products';
import RevealAds from './../../components/RevealAds';
import Cards from './../../components/Cards/Cards';
import FadeLoader from 'react-spinners/FadeLoader';

export default class Home extends Component{
	constructor(props){
		super(props);
		this.state = {
			_page: 1,
			limit: 20,
			productList: [],
		    isLoading: false,
		    endProduct: "",
		    adStop: false,
		    createImageRandom: "",
		    randomNumber: []
		};
		this.filterDetails = this.filterDetails.bind(this);
		window.onscroll = () => {
	      const {
	        loadUsers,
	        state: {
	          isLoading
	        },
	      } = this;
	      if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
	        this.loadProducts();
	      }
	    };
	}

	componentWillMount() {
	    this.loadProducts();

	}

	createRandomeImageId(){
		let rdnNumber = Math.floor(Math.random() * 1000);
		return rdnNumber;
	}

  	loadProducts(){
	    if(this.state.productList.length === 0){
	    	let Url = "/api/products?_page=" + this.state._page + "&_limit=" + this.state.limit;
		    this.setState({ isLoading: true }, () => {
			    axios.get(Url)
			      	.then(res => {
			        	this.state.productList.push(res.data);
			        	this.setState({
			      		productList: [
			              ...this.state.productList
			            ]
			      	})
			    })
		    });
	    }else{
	    	this.setState({
	    		_page: this.state._page + 1
	    	}, () => {
	    		let fetchProduct = "/api/products?_page=" + this.state._page + "&_limit=" + this.state.limit;
	    		this.setState({ isLoading: true }, () => {
			    axios.get(fetchProduct)
			      	.then(res => {
			      		{(res.data.length === 0) ?
		      			this.setState({
		      				endProduct: "~ end of catalogue ~"
		      			}):
			      		res.data.map((now, idx) => {
					      	this.state.productList[0].push(now);
					        	this.setState({
					      		productList: [
					              ...this.state.productList
					            ]
					      	})
			      		})}
			    })
		    });
	    	})


	    	let rdnNumber = this.createRandomeImageId();
	    	if(this.state.randomNumber.length === 0){
	    		this.state.randomNumber.push(rdnNumber);
		    	this.setState({
		    		randomNumber: [
		              ...this.state.randomNumber
		            ]
		    	}, () => {
		    		let imageUrl = "/ads/?r=" + rdnNumber;
			    	this.setState({
			    		createImageRandom: imageUrl
			    	})
			    })
	    	}else if(rdnNumber){
	    		if(this.state.randomNumber.includes(rdnNumber) === true){
	    			this.createRandomeImageId();
	    		}else{
		    		this.state.randomNumber.push(rdnNumber);
			    	this.setState({
			    		randomNumber: [
			              ...this.state.randomNumber
			            ]
			    	}, () => {
			    		let imageUrl = "/ads/?r=" + rdnNumber;
				    	this.setState({
				    		createImageRandom: imageUrl
				    	})
			    	})
	    		}
	    	}
	    	




	    	
    	}
    }


    filterDetails(sortFilter){
    	let sizeValue = JSON.stringify(sortFilter['Size']);
    	let priceValue = JSON.stringify(sortFilter['Price']);
    	let dateValue = JSON.stringify(sortFilter['Date']);
    	let fromSize, toSize, fromPrice, toPrice, fromDate, toDate;
    		
    	if(sizeValue){
    	    fromSize = sizeValue.substring(1, 3);
		    toSize = sizeValue.substring(4, 6);
    	}else if(priceValue){
    	    fromPrice = priceValue.substring(1, 3);
		    toPrice = priceValue.substring(4, 6);
    	}else if(dateValue){
    	    fromDate = dateValue.substring(1, 3);
		    toDate = dateValue.substring(4, 6);
    	}
    	

		if(fromSize && toSize){
			var filteredProduct = this.state.productList[0].filter(function(number) {
				if(number.size > fromSize && number.size < toSize){
					return number;
				}
			})
			if(filteredProduct){
				this.state.productList = [];
				this.state.productList.push(filteredProduct);
				this.setState({
					productList: [
		               ...this.state.productList
		            ]
				})	
			}else if(filteredProduct.length === 0){
				this.setState({
					adStop: true,
		      		endProduct: "~ end of catalogue ~"
		      	})
			}
		}
		else if(fromPrice && toPrice){
			var filteredProduct = this.state.productList[0].filter(function(number) {
				if(parseFloat(number.price/100) > fromPrice && parseFloat(number.price/100) < toPrice){
					return number;
				}
			})
			if(filteredProduct){
				this.state.productList = [];
				this.state.productList.push(filteredProduct);
				this.setState({
					productList: [
		               ...this.state.productList
		             ]
				})
			}else if(filteredProduct.length === 0){
				this.setState({
					adStop: true,
		      		endProduct: "~ end of catalogue ~"
		      	})
			}
		}
    }


    sortAsc(value){
    	if(value === "price"){
	    	this.state.productList[0].sort(function(a, b) {
	    		return parseFloat(b.price) - parseFloat(a.price);
			});
    	}else if(value === "size"){
    		this.state.productList[0].sort(function(a, b) {
	    		return parseFloat(b.size) - parseFloat(a.size);
			});
    	}
    	
		this.setState({
			productList: [
              ...this.state.productList
            ],
            adStop: true
		})
    }

    sortDsc(value){
    	if(value === "price"){
	    	this.state.productList[0].sort(function(a, b) {
	    		return parseFloat(a.price) - parseFloat(b.price);
			});
    	}else if(value === "size"){
    		this.state.productList[0].sort(function(a, b) {
	    		return parseFloat(a.size) - parseFloat(b.size);
			});
    	}
    	
		this.setState({
			productList: [
              ...this.state.productList
            ],
            adStop: true
		})
    }

	render(){
		return(
			<div className="home-section">
				<Header />
				<div className="pl-3 pr-3 pb-3">
					<div className="row">
						<div className="col-md-2 col-lg-2 filterDesign pt-4">
							<Filters productList={this.state.productList} filterDetails={this.filterDetails} />
						</div>
						<div className="col-md-10 col-lg-10 cardDesign">
							<div className="row container">
								<div className="col-md-3 col-lg-3 text-center p-3">Face</div>
								<div className="col-md-3 col-lg-3 text-center p-3">
								Price 
									<div>
										<img className="img-selection" src="../assets/up.svg" onClick={this.sortAsc.bind(this, "price")} />
										<img className="img-selection" src="../assets/down.svg" onClick={this.sortDsc.bind(this, "price")} />
									</div>
								</div>
								<div className="col-md-3 col-lg-3 text-center p-3">Size
									<div>
										<img className="img-selection" src="../assets/up.svg" onClick={this.sortAsc.bind(this, "size")} />
										<img className="img-selection" src="../assets/down.svg" onClick={this.sortDsc.bind(this, "size")} />
									</div>
								</div>
								<div className="col-md-3 col-lg-3 text-center p-3">Date</div>
							</div>
							
							{(this.state.productList.length > 0) ? 
								<Cards  products={this.state.productList}
										endProduct={this.state.endProduct}
										adStop={this.state.adStop}
										createImageRandom={this.state.createImageRandom} /> : 
								<div className="d-flex justify-content-center">
								 <FadeLoader
							          className="overrideLoader"
							          sizeUnit={"px"}
							          size={150}
							          color={'#68509f'}
							          loading={true}
							        />
								</div>
							}
						</div>
					</div>
					</div>
			</div>
		);
	}
	
}