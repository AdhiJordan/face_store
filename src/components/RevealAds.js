import React, { Component } from 'react';
import axios from 'axios';
export default class RevealAds extends Component {

    constructor(props){
        super(props);
        this.state = {
            getImage: ""
        }
    };


    render() {
    	let customStyle = {};
        if (this.props.customWidth) {
            customStyle = {
                width: this.props.customWidth
            };
        }

        if (this.props.customStyle) {
            Object.assign(customStyle, this.props.customStyle)
        }


        return (
             <div className={"w3-modal  " + this.props.openCls}>
                <div className="w3-modal-content w3-center p-5" style={customStyle}>
                    {this.props.children}
                </div>
                 
            </div>
        );
    }
}