import React, {Component} from 'react';
import FadeLoader from 'react-spinners/FadeLoader';
import RevealAds from './../RevealAds';


export default class Cards extends Component{
	constructor(props){
		super(props);
		this.state = {
			listProduct: [],
			getImage: "",
			modalAds: 'w3-show',
			months: {
			    'Jan' : '01',
			    'Feb' : '02',
			    'Mar' : '03',
			    'Apr' : '04',
			    'May' : '05',
			    'Jun' : '06',
			    'Jul' : '07',
			    'Aug' : '08',
			    'Sep' : '09',
			    'Oct' : '10',
			    'Nov' : '11',
			    'Dec' : '12'
			}
		};
		this.getDate = this.getDate.bind(this);
	}

    componentWillReceiveProps(newProps){
    	if(newProps){
    		if(this.props.products[0].length % 20 === 0){
				this.setState({
		            modalAds: 'w3-show'
		        }, () => {
			        this.setState({
			            getImage: newProps.createImageRandom,
			            modalAds: 'w3-show'
			        })
		        })

			}
    	}
    }


    closeModal(){
        this.setState({
        	modalAds: "w3-hide"
        }, () => {
        	this.setState({
        		getImage: ""
        	})
        })
    }


	getDate(date){
		let formattingDate = date.substr(4, 12).split(' ').join('-');
		let getMonthType = formattingDate.substr(0, 3);
		let getMonth = formattingDate.replace(/^.{3}/g, this.state.months[getMonthType]).slice(0, -1);
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();
		var date1 = new Date(getMonth);
		var date2 = new Date(mm + "-" + dd + "-" + yyyy);
		var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
		if(diffDays < 7){
			return diffDays + " days ago";
		}
		return getMonth ; 

	}

	render(){
		return(
			<div className="Cards-section container">
				{(this.props.products) ?
					this.props.products[0].map((product, idx) => {
						return(
							<div className="row car-design" key={idx}>
								<div className="col-md-12 col-lg-12 row mb-4">
									<div className="col-md-3 col-lg-3 text-center">{product.face}
										<img src="./assets/bodyDesign.jpg" alt="body" className="body-design" />
									</div>
									<div className="col-md-3 col-lg-3 text-center pt-5">{(product.price/100 + "$")}</div>
									<div className="col-md-3 col-lg-3 text-center pt-5">{product.size}</div>
									<div className="col-md-3 col-lg-3 text-center pt-5">{this.getDate(product.date)}</div>
								</div>
							</div>
						);
					})

				 : <p>loading</p>}
				 <div className="d-flex justify-content-center">
				 {((this.props.products[0].length % 20) === 0 && this.props.products[0].length !== 20 && this.props.adStop !== true) ? 
				 	<RevealAds openCls={this.state.modalAds} customWidth="550px">
				 		{(this.state.getImage !== null) ?
				 			<div>
				 			<h3>Here you're sure to find a bargain on some of the finest ascii available to purchase. Be sure to peruse our selection of ascii faces in an exciting range of sizes and prices.</h3>
            				<h3>But first, a word from our sponsors:</h3> 
				 			<img className="w3-center" src={this.state.getImage} />
				 			<span onClick={this.closeModal.bind(this)}
                			className="w3-button w3-display-topright">&times;</span>
				 			</div> :
				 			null				 		}
				 	</RevealAds> :
				 	null
				 }
				 <FadeLoader
		          className="overrideLoader"
		          sizeUnit={"px"}
		          size={150}
		          color={'#68509f'}
		          loading={true}
		        />
		        </div>
				<p className="text-center">{this.props.endProduct}</p>
			</div>
		);
	}
}