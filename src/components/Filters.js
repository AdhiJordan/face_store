import React, {Component} from 'react';
import sortList from './data/sortCriteria.json';
import styles from './styles.scss';
import Products from './Products';
import RevealAds from './RevealAds';
import axios from 'axios';
import Cards from './Cards/Cards';
import FadeLoader from 'react-spinners/FadeLoader';

export default class Filters extends Component{
	constructor(props){
		super(props);
		this.state = {
			sortList: sortList,
			sort: {},
			endProduct: "",
		    adStop: false,
		    _page: 1,
			limit: 20,
			productList: [],
		};
	}



	toggleFilter(sortData, sortValue){
		if(sortData && sortValue){
			this.state.sort[sortData] = sortValue;
			this.setState({
				sort: this.state.sort
			})
		}
		this.props.filterDetails(this.state.sort);
	}

	render(){
		return(
			<div className="filters-section">
				{(this.state.sortList.map((sort, idx) => {
					return(
						<div key={idx} className="pt-3 pb-3">
							<h5>{sort.value}</h5>
							{(sort.range.map((rangeList, index) => {
								return(
									<div className="pl-3" key={index}>
										<input type="checkbox"
	                                    className="s7d1-st-checkbox pt-3"
	                                    id={`${rangeList.one}_check_${idx}`}
	                                    value={rangeList.one}
	                                    checked={(rangeList.one === this.state.sort[sort.value] && Object.keys(this.state.sort).map((data) => { if(data === sort.value) {return true;} }) ) ? true : false}
	                                    onChange={this.toggleFilter.bind(this, sort.value, rangeList.one)} />
		                                <label htmlFor={`${rangeList.one}_check_${idx}`}>
		                                    {rangeList.one}
		                                </label>
									</div>
								);
							}))}
							 <hr />
						</div>
					);
				}))}

				{(this.state.productList.length > 0) ? 
					<Cards products={this.state.productList} endProduct={this.state.endProduct} adStop={this.state.adStop} /> : 
					null
				}
			</div>
		);
	}
	
}
