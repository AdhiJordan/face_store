import React, {Component} from 'react';
import axios from 'axios';
import Cards from './Cards/Cards';

export default class Products extends Component{
	constructor(props){
		super(props);
		this.state = {
			_page: 1,
			limit: 15,
			productList: [],
		    isLoading: false
		};
	    window.onscroll = () => {
	      const {
	        loadUsers,
	        state: {
	          isLoading
	        },
	      } = this;
	      if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
	        this.loadProducts();
	      }
	    };
	}


	componentWillMount() {
	    this.loadProducts();
	}

  	loadProducts(){
	    if(this.state.productList.length === 0){
	    	let Url = "/api/products?_page=" + this.state._page + "&_limit=" + this.state.limit;
		    this.setState({ isLoading: true }, () => {
			    axios.get(Url)
			      	.then(res => {
			        	this.state.productList.push(res.data);
			        	this.setState({
			      		productList: [
			              ...this.state.productList
			            ]
			      	})
			    })
		    });
	    }else{
	    	this.setState({
	    		_page: this.state._page + 2
	    	}, () => {
	    		let loadingUrl = "/api/products?_page=" + this.state._page + "&_limit=" + this.state.limit;
	    		this.setState({ isLoading: true }, () => {
			    axios.get(loadingUrl)
			      	.then(res => {
			      		res.data.map((now, idx) => {
					      	this.state.productList[0].push(now);
					        	this.setState({
					      		productList: [
					              ...this.state.productList
					            ]
					      	})
			      		})
			    })
		    });
	    	})

    	}
    }


	render(){
	const {
      isLoading,
      productList,
    } = this.state;
		return(
			<div>
				{(this.state.productList.length > 0) ? 
					<Cards products={this.state.productList} /> : null
				}
			</div>
		);
	}
	
}